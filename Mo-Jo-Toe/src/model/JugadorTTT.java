package model;


import java.util.ArrayList;
import java.util.List;





public class JugadorTTT
{
	private List<Integer> movimientosJugador;
	private boolean estadoJugador;
	private String nombre;
		
	public JugadorTTT(String nombre)
	{
		estadoJugador= true;
		this.nombre = nombre;
		this.movimientosJugador = new ArrayList<Integer>();
	}
	public void setEstadoJugador(boolean estadoJugador)
	{
		this.estadoJugador = estadoJugador;
	}
	public boolean getEstadojugador()
	{
		return this.estadoJugador;
	}
	public void jugar(Integer posicionJugada)
	{
		
			movimientosJugador.add(posicionJugada);
		
	}
	public ArrayList<Integer> getJugadas()
	{	
		return (ArrayList<Integer>) movimientosJugador;
	}

	public String getNombre()
	{
		return nombre;
	}
}
