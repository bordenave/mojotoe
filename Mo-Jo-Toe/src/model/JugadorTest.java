package model;

import static org.junit.Assert.*;

import org.junit.Test;

import static org.junit.Assert.*;

import org.junit.Test;

import model.JugadorTTT;

public class JugadorTest 
{

	@Test
	public void testJugador() 
	{
		JugadorTTT jugador= new JugadorTTT("nombre");
		
		assertTrue(jugador.getEstadojugador());
	}

	@Test
	public void testJugadorString()
	{
		JugadorTTT jugador = new JugadorTTT("nombre");
		
		assertTrue(jugador.getEstadojugador());
		assertTrue("nombre" == jugador.getNombre());
	}

	@Test
	public void testJugar()
	{
		JugadorTTT jugador = new JugadorTTT("nombre");
		
		jugador.jugar(3);
		
		assertTrue(1 == jugador.getJugadas().size());
		
	}
	


	
	@Test
	public void testJugarPosiciones()
	{
		
		JugadorTTT jugador = new JugadorTTT("nombre");
		
		jugador.jugar(0);
		jugador.jugar(1);
		jugador.jugar(2);
		jugador.jugar(3);
		jugador.jugar(4);
		jugador.jugar(5);
		jugador.jugar(6);
		jugador.jugar(7);
		jugador.jugar(8);
		
		assertEquals(9 , jugador.getJugadas().size());
		
	}
	

	@Test
	public void testGetNombre() 
	{
		JugadorTTT jugador = new JugadorTTT("nombre");
		
		assertEquals("nombre", jugador.getNombre());
	}

	
	
}

