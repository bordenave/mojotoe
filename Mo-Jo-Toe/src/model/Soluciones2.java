package model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class Soluciones2 
{
	private ArrayList<List> soluciones;
	
	private List<Integer> solucionParticular0;
	private List<Integer> solucionParticular1;
	private List<Integer> solucionParticular2;
	private List<Integer> solucionParticular3;
	private List<Integer> solucionParticular4;
	private List<Integer> solucionParticular5;
	private List<Integer> solucionParticular6;
	private List<Integer> solucionParticular7;
	private List<Integer> solucionParticular8;
	private List<Integer> solucionParticular9;
	private List<Integer> solucionParticular10;
	private List<Integer> solucionParticular11;
	//private ArrayList<Integer> solucionParticular12;
	
	
	
	public Soluciones2() 
	{
		soluciones = new ArrayList<List>();
		
		iniciarSolucionesParticulares();
		
		
		agregarSolucionesParticulares();
		
	}
	
	private void agregarSolucionesParticulares() {
		soluciones.add(solucionParticular0);
		soluciones.add(solucionParticular1);
		soluciones.add(solucionParticular2);
		soluciones.add(solucionParticular3);
		soluciones.add(solucionParticular4);
		soluciones.add(solucionParticular5);
		soluciones.add(solucionParticular6);
		soluciones.add(solucionParticular7);
		soluciones.add(solucionParticular8);
		soluciones.add(solucionParticular9);
		soluciones.add(solucionParticular10);
		soluciones.add(solucionParticular11);
	}
	private void iniciarSolucionesParticulares() 
	{
		this.solucionParticular0 =  Arrays.asList(0,1,2);
		this.solucionParticular1 =  Arrays.asList(3,4,5);
		this.solucionParticular2 =  Arrays.asList(6,7,8);
		this.solucionParticular3 =  Arrays.asList(0,3,6);
		this.solucionParticular4 =  Arrays.asList(1,4,7);
		this.solucionParticular5 =  Arrays.asList(2,5,8);
		this.solucionParticular6 =  Arrays.asList(2,4,6);
		this.solucionParticular7 =  Arrays.asList(8,1,3);
		this.solucionParticular8 =  Arrays.asList(5,7,0);
		this.solucionParticular9 =  Arrays.asList(6,1,5);
		this.solucionParticular10 =  Arrays.asList(0,4,8);
		this.solucionParticular11 =  Arrays.asList(3,7,2);
		
	}
	
	private ArrayList<List> getSoluciones()
	{
		return soluciones;
	}
	public boolean comprobarSolucion(ArrayList<Integer> jugadas)
	{				
			for(List<?> solucionesParticulares : this.soluciones)
			{
				if(jugadas.containsAll(solucionesParticulares))
				{
					return true;
				}
			}
			return false;				
	}
}
