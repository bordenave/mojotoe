package model;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

public class Soluciones2Test {

	@Test
	public void test() 
	{	
		Soluciones2 solucion = new Soluciones2();
		ArrayList<Integer> jugadas = new ArrayList();
		jugadas.add(1);
		jugadas.add(0);
		jugadas.add(2);
		
		
		assertTrue(solucion.comprobarSolucion(jugadas));	
	}

	
	@Test
	public void test2() 
	{	
		Soluciones2 solucion = new Soluciones2();
		ArrayList<Integer> jugadas = new ArrayList();
		jugadas.add(1);
		jugadas.add(3);
		jugadas.add(2);
		
		
		assertFalse(solucion.comprobarSolucion(jugadas));	
	}
	
	@Test
	public void test3() 
	{	
		Soluciones2 solucion = new Soluciones2();
		ArrayList<Integer> jugadas = new ArrayList();
		jugadas.add(1);
		jugadas.add(5);
		jugadas.add(9);
		
		
		assertFalse(solucion.comprobarSolucion(jugadas));	
	}
}
