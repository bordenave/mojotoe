package model;

import java.util.ArrayList;
import java.util.HashMap;

public class Tablero 
{
	private JugadorTTT jugador1;
	private JugadorTTT jugador2;
	private Integer tamano;
	private ArrayList<Integer> casilleroXorden;
	private HashMap <Integer, String> jugadorXcasillero;
	private String ganador;
	private boolean estadoTablero;
	private Soluciones2 soluciones;
	private ArrayList<Integer> casillerosPosibles;
	
	public Tablero()
	{
		setTablero(9);
		setTableroActivo(true);
		inciarRegistroJugadas();
		iniciarJugadores("gorillaX" , "gorillaZ");
		soluciones = new Soluciones2();	
		
	}
	public void setjugador1Nombre(String nombre)
	{
		jugador1 = new JugadorTTT(nombre);
		if(jugador2.getEstadojugador())
		{
			jugador1.setEstadoJugador(false);
		}
	}
	public void setjugador2Nombre(String nombre)
	{
		jugador2 = new JugadorTTT(nombre);
		if(jugador1.getEstadojugador())
		{
			jugador2.setEstadoJugador(false);
		}
	}
	public Integer tamañoTablero()
	{
		return tamano;
	}
	
	public ArrayList<Integer> getJugadasEchas() 
	{
		return casilleroXorden;
	}
	
	public void setJugada(Integer casilleroJugado)
	{
		if(this.estadoTablero && !casilleroXorden.contains(casilleroJugado) && casillerosPosibles.contains(casilleroJugado))
		{
			
			JugadorTTT jugadorActual = obtenerProximoJugador();
			jugadorActual.jugar(casilleroJugado);
			casilleroXorden.add(casilleroJugado);
			
			
			jugadorXcasillero.put(casilleroJugado, jugadorActual.getNombre());
			
			if(soluciones.comprobarSolucion(jugadorActual.getJugadas()) )
			{
				this.setTableroActivo(false);
				ganador = jugadorActual.getNombre();
			}	
		}
		else 
		{
			throw new IllegalArgumentException();
		}		
	}	
	
	public String proximoJugador()
	{
		if(!jugador1.getEstadojugador() && jugador2.getEstadojugador())
		{
		
			return jugador1.getNombre();
		}
		else if(jugador1.getEstadojugador() && !jugador2.getEstadojugador())
		{
			return jugador2.getNombre();
		}
		else 
		{
			throw new IllegalArgumentException();
		}
	}
	
	public String ganador() 
	{
		return ganador;
	}
	
	private JugadorTTT obtenerProximoJugador() 
	{
		if(!jugador1.getEstadojugador() && jugador2.getEstadojugador())
		{
			jugador1.setEstadoJugador(true);
			jugador2.setEstadoJugador(false);
			return jugador1;
		}
		else if(jugador1.getEstadojugador() && !jugador2.getEstadojugador())
		{
			jugador1.setEstadoJugador(false);
			jugador2.setEstadoJugador(true);
			return jugador2;
		}
		else 
		{
			throw new NullPointerException();
		}
	}
	
	private void setTableroActivo(boolean estadoTablero) 
	{
		this.estadoTablero = estadoTablero;
	}
	
	private void iniciarJugadores(String nombre1 , String nombre2 ) 
	{
		this.jugador1 = new JugadorTTT(nombre1);
		this.jugador2 = new JugadorTTT(nombre2);

		jugador1.setEstadoJugador(false);
		jugador2.setEstadoJugador(true);
	}
	
	private void setTablero(int casilleros)
	{
		this.tamano = casilleros;
	}
	
	private void inciarRegistroJugadas() 
	{
		casilleroXorden = new ArrayList<Integer>();
		jugadorXcasillero = new HashMap< Integer ,String>();
		casillerosPosibles = new ArrayList<Integer>();
		 for(int i=0; i< tamano; i++)
		 {
			 casillerosPosibles.add(i);
		 }
	}
}
