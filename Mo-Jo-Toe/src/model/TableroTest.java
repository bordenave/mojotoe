package model;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TableroTest
{
	Tablero tablero;
	@Before
	public void iniciarTablero()
	{
		tablero = new Tablero();
		tablero.setjugador1Nombre("juan");
		tablero.setjugador2Nombre("luis");
	
	}
	@Test
	public void testproximoJugador()
	{			
		assertTrue("juan" == tablero.proximoJugador());	
	}
	
	@Test
	public void testproximoJugadorFalso()
	{	
		assertFalse("luis" == tablero.proximoJugador());	
	}

	@Test 
	public void ganadorNull()
	{
		assertNull(tablero.ganador());
	}
	
	@Test
	public void hacerPrimerJugadaProximoJugador()
	{

		tablero.setJugada(1);		
		assertTrue("luis" == tablero.proximoJugador());
		
	}
	@Test
	public void hacerSegundaJugadaProximoJugador()
	{

		tablero.setJugada(1);
		tablero.setJugada(2);
		
		assertTrue("juan" == tablero.proximoJugador());	
	}
	@Test
	public void hacerSegundaJugadaProximoJugadorEquivocado()
	{

		tablero.setJugada(1);
		tablero.setJugada(2);
		
		assertFalse("luis" == tablero.proximoJugador());	
	}
	@Test
	public void ganaJugado1()
	{

		tablero.setJugada(1);
		tablero.setJugada(4);
		tablero.setJugada(3);
		tablero.setJugada(8);
		tablero.setJugada(0);
		tablero.setJugada(2);
		tablero.setJugada(6);
			
		assertTrue("juan" == tablero.ganador());	
	}
	@Test
	public void ganaJugado2()
	{

		tablero.setJugada(1);
		tablero.setJugada(4);
		tablero.setJugada(3);
		tablero.setJugada(8);
		tablero.setJugada(6);
		tablero.setJugada(0);
			
		assertTrue("luis" == tablero.ganador());	
	}
	@Test
	public void ganaJugadorToroidal()
	{

		tablero.setJugada(1);
		tablero.setJugada(4);
		tablero.setJugada(3);
		tablero.setJugada(5);
		tablero.setJugada(8);
			
		assertTrue("juan" == tablero.ganador());	
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void jugadaIgualCasillero()
	{
		tablero.setJugada(1);
		tablero.setJugada(1);
	}
	@Test(expected = IllegalArgumentException.class)
	public void jugarConJuegoTerminado()
	{

		tablero.setJugada(1);
		tablero.setJugada(4);
		tablero.setJugada(3);
		tablero.setJugada(5);
		tablero.setJugada(8);
		tablero.setJugada(2);
			
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void jugarConCasilleroInexistente()
	{

		tablero.setJugada(1);
		tablero.setJugada(4);
		tablero.setJugada(3);
		tablero.setJugada(5);
		
		
		tablero.setJugada(9);
				
	}
	@Test(expected = IllegalArgumentException.class)
	public void jugarConCasilleroInexistenteNegativo()
	{

		tablero.setJugada(1);
		tablero.setJugada(4);
		tablero.setJugada(3);
		tablero.setJugada(5);
		
		
		tablero.setJugada(-1);
				
	}
}
