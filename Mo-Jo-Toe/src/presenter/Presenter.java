package presenter;


import model.Tablero;

public class Presenter
{	
		private Tablero tablero;
		private String jugador1;
		private String jugador2;

		public Presenter()
		{
			tablero= new Tablero( );	 
		}
		public String getNombreJugador1() 
		{
			return jugador1;
		}
		
		public String getNombreJugador2() 
		{
			return jugador2;
		}
		
		public void setJugadores(String jugador1 , String jugador2)
		{
			
			if(!(jugador1.equals(jugador2)) )
			{
				this.jugador1 = jugador1;
				tablero.setjugador1Nombre(jugador1);
				
				this.jugador2 = jugador2;
				tablero.setjugador2Nombre(jugador2);
			}
			else
			{
				this.jugador1 = "jugador1";
				tablero.setjugador1Nombre("jugador1");
				
				this.jugador2 = "jugador2";
				tablero.setjugador2Nombre("jugador2");
			}
			
		}
		
		public boolean agregarJugada(int posicion)
		{
			try
			{
				if(  tablero.ganador()== null)
				{
					tablero.setJugada(posicion);
					return true;
				}
			}	
			catch(Exception e)
			{
				return false;
			}
			return false;
		}
		public String getGanador()
		{
			if(tablero.ganador()!=null)
			{
				return tablero.ganador();
			}
			else
			{
				throw new NullPointerException();
			}
			
		}
		public String ganadorImagen()
		{
			if(tablero.ganador()!=null  && tablero.ganador().equals(jugador1))
			{
				return "jugador1";
			}
			else if(tablero.ganador()!=null  && jugador2.equals(tablero.ganador()))
			{
				return "jugador2";
			}
			return null;
		}
		
		public String proximoJugadorImagen()
		{
			if(tablero.proximoJugador().equals(jugador1))
			{
				return "jugador1";
			}
			else if(tablero.proximoJugador().equals(jugador2))
			{
				return "jugador2";
			}
			return null;
		}
}
