package view;

import javax.swing.Icon;

public class ImageNText
{
	private Icon img;
	private String name;

	public ImageNText(Icon img, String name)
	{
		this.img=img;
		this.name=name;
	}


	public Icon getImg() {
		return img;
	}

	public void setImg(Icon img) {
		this.img = img;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
