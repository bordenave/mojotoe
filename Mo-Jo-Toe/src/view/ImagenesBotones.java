package view;

import java.awt.Color;
import java.util.ArrayList;

import javax.swing.ImageIcon;

import presenter.Presenter;

public class ImagenesBotones 
{
	private ImageIcon Noodle;
	private ImageIcon Murdoc;
	private ImageIcon _2_D;
	private ImageIcon Russel;
	private ImageIcon jugador1Imagen;
	private ImageIcon jugador2Imagen;
	private ArrayList<ImageIcon> personajes;
	private Presenter presenterLocal;
	
	public ImagenesBotones(Presenter presenter)
	{
		 presenterLocal=presenter;
		 definirImagenes();
		 cagarPersonajes();
			
		 jugador1Imagen = null;
		 jugador2Imagen = null;
	}
	
	
	private void definirImagenes()
	{
		Noodle=new ImageIcon(this.getClass().getResource("/Noodle_boton.png"));
		Murdoc=new ImageIcon(this.getClass().getResource("/Murdoc_boton.png"));
		_2_D=new ImageIcon(this.getClass().getResource("/2-D_boton.png"));
		Russel=new ImageIcon(this.getClass().getResource("/russel_boton.png"));		
	}
	private void cagarPersonajes() 
	{
		personajes= new ArrayList<ImageIcon>();
		personajes.add(_2_D);
		personajes.add(Murdoc);
		personajes.add(Noodle);
		personajes.add(Russel);
	}	
	public Color getColorActual() 
	{
		if(presenterLocal.proximoJugadorImagen().equals("jugador1"))return Color.MAGENTA;
		if(presenterLocal.proximoJugadorImagen().equals("jugador2"))return Color.YELLOW;
		return Color.green;
	}
	
	public ImageIcon ganadorImagen()
	{
		
		if( presenterLocal.ganadorImagen().equals("jugador1"))
		{
			return jugador1Imagen;
		}
		 if (presenterLocal.ganadorImagen().equals("jugador2"))
		{
			return jugador2Imagen;
		}
		else
		{
			throw new NullPointerException();
		}		
	}
	public void setImagenJugador1(int indice )
	{
		jugador1Imagen =  personajes.get(indice);
	}
	public void setImagenJugador2(int indice )
	{
		jugador2Imagen =  personajes.get(indice);
	}
	public ImageIcon imagenjugadorActual()
	{
		if(presenterLocal.proximoJugadorImagen().equals("jugador1")) 
		{
			return jugador2Imagen;
		}
		else if(presenterLocal.proximoJugadorImagen().equals("jugador2"))
		{
			return jugador1Imagen;
		}
		throw new NullPointerException();	
	}

}
