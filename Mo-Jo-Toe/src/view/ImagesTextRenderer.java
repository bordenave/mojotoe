package view;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

public class  ImagesTextRenderer extends JLabel implements ListCellRenderer
{
	@Override 
	public Component getListCellRendererComponent(JList list, Object val,
				int index, boolean selected, boolean focused) 
{
		ImageNText it = (ImageNText) val;
		
		setIcon(it.getImg());
		setText(it.getName());
		
		if(selected)
		{
			setBackground(list.getSelectionBackground());
			setForeground(list.getSelectionForeground());
		}else
		{
			setBackground(list.getBackground());
			setForeground(list.getForeground());
		}
		setFont(list.getFont());		

				return this;
	}

}
