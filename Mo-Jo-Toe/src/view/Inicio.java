package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import presenter.Presenter;

public class Inicio extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField textField;
	private JTextField textField_1;
	private final JComboBox<ImageNText> comboBox;
	private final Presenter presenterLocal;
	

	
	public Inicio(JFrame parent,  Presenter presenter, ImagenesBotones imagenesBt) 
	{
		
		super(parent,true);
		presenterLocal = presenter;
		this.setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
		setTitle("MO-JO-TOE");
		setAlwaysOnTop(true);
		setResizable(false);
		setBounds(100, 100, 538, 370);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 0, 5, 0));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setBackground(new Color(0, 0, 0));
		contentPanel.setLayout(null);
		setLocationRelativeTo(null);
		

		
		comboBox = new JComboBox();	
		comboBox.setBounds(120, 201, 120, 34);	
		comboBox.setModel(populate());
		comboBox.setSelectedIndex(3);
		comboBox.setRenderer(new ImagesTextRenderer());
		contentPanel.add(comboBox);
		imagenesBt.setImagenJugador1(comboBox.getSelectedIndex());
		
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setBounds(120, 265, 120, 34);	
		comboBox_1.setModel(populate());
		comboBox_1.setSelectedIndex(2);
		comboBox_1.setRenderer(new ImagesTextRenderer());
		contentPanel.add(comboBox_1);
		imagenesBt.setImagenJugador2(comboBox_1.getSelectedIndex());
		
		textField = new JTextField("gorillaX");
		textField.setBounds(248, 215, 148, 20);
		contentPanel.add(textField);
		textField.setColumns(10);
		

		
		textField_1 = new JTextField("gorillaZ");
		textField_1.setBounds(248, 279, 148, 20);
		contentPanel.add(textField_1);
		textField_1.setColumns(10);
		
		
		presenter.setJugadores(textField.getText(), textField_1.getText());
		
		JLabel ImagenInicio = new JLabel("");
		ImagenInicio.setIcon(new ImageIcon(this.getClass().getResource("/mojo_inicio.png")));
		ImagenInicio.setBounds(0, 0, 540, 170);
		contentPanel.add(ImagenInicio);
		
		JLabel firma = new JLabel("");
		
        
		firma.setIcon( new ImageIcon(this.getClass().getResource("/signature.png")));
		firma.setBounds(470, 93, 540, 188);
		contentPanel.add(firma);
		
		final JLabel lbNombre1 = new JLabel("Nombre Jugador");
		lbNombre1.setBounds(248, 198, 126, 14);
		lbNombre1.setForeground(Color.YELLOW);
		
		contentPanel.add(lbNombre1);
		
		final JLabel lblNewLabel_2 = new JLabel("Nombre Jugador");
		lblNewLabel_2.setBounds(248, 261, 126, 14);
		lblNewLabel_2.setForeground(Color.MAGENTA);
		contentPanel.add(lblNewLabel_2);
		
		
		
		{
			JPanelOK buttonPane = new JPanelOK(presenter, imagenesBt,textField,textField_1,comboBox,comboBox_1);
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			buttonPane.setBackground(Color.BLACK);
			
			

			
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("GO!");
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
				okButton.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent arg0)
					{
						
						buttonPane.setNombre();;
						dispose();
					
						
					}
				
				});

			}
		}
	}



	private DefaultComboBoxModel<ImageNText> populate()
	{
		DefaultComboBoxModel<ImageNText> dm = new DefaultComboBoxModel<ImageNText>();
		dm.addElement(new ImageNText(new ImageIcon(this.getClass().getResource("/2-Ds.png")),"2-D"));
		dm.addElement(new ImageNText(new ImageIcon(this.getClass().getResource("/Murdocs.png")),"Murdoc"));
		dm.addElement(new ImageNText(new ImageIcon(this.getClass().getResource("/Noodles.png")),"Noodle"));
		dm.addElement(new ImageNText(new ImageIcon(this.getClass().getResource("/russels.png")),"Russel"));
			return dm;

	}
}






