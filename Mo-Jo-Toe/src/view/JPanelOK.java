package view;

import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTextField;

import presenter.Presenter;

public class JPanelOK extends JPanel
{
	private Presenter localPresenter;
	private ImagenesBotones imagenesBt;
	private JTextField text1;
	private JTextField text2;
	private JComboBox combo1;
	private JComboBox combo2;
	
	public JPanelOK(Presenter presenter, ImagenesBotones imagenesBtLc, JTextField text1, JTextField text2, JComboBox combo1, JComboBox combo2 )
	{
		localPresenter =presenter;
		imagenesBt=imagenesBtLc;
		this.text1=text1;
		this.text2=text2;
		this.combo1=combo1;
		this.combo2=combo2;
		
	}
	public void setNombre()
	{
		localPresenter.setJugadores(text1.getText(), text2.getText());
		imagenesBt.setImagenJugador1(combo1.getSelectedIndex());
		imagenesBt.setImagenJugador2(combo2.getSelectedIndex());
	}
}