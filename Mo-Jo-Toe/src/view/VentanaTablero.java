package view;

import java.awt.Button;
import java.awt.Component;
import java.awt.EventQueue;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import java.awt.Color;
import java.awt.Window.Type;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;
import javax.swing.border.MatteBorder;
import presenter.Presenter;

public class VentanaTablero 
{

	private JFrame frmMojotoe;
	private Presenter  presenter;
	private List<JButton> listaBotones;
	private ImagenesBotones imagenesBt;	
	
	public static void main(String[] args) 
	{
		try 
		{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e1) 
		{
			e1.printStackTrace();
		} catch (InstantiationException e1) 
		{
			e1.printStackTrace();
		} catch (IllegalAccessException e1) 
		{	
			e1.printStackTrace();
		} catch (UnsupportedLookAndFeelException e1) 
		{
			e1.printStackTrace();
		}
		EventQueue.invokeLater(new Runnable() 
		{
			public void run() 
			{
				try
				{
					VentanaTablero window = new VentanaTablero();
					window.frmMojotoe.setVisible(true);	
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	public VentanaTablero() 
	{
		initialize();	
	}
	private void initialize() 
	{
		presenter = new Presenter();
		imagenesBt = new ImagenesBotones(presenter);
		Inicio ventanaInicio =new Inicio(frmMojotoe, presenter , imagenesBt);
		ventanaInicio.setVisible(true);
		
		
		frmMojotoe = new JFrame();
		frmMojotoe.setAlwaysOnTop(true);
		frmMojotoe.setType(Type.UTILITY);
		frmMojotoe.setResizable(false);
		ImageIcon  fondo_tablero =  new ImageIcon(this.getClass().getResource("/fondo_tablero.jpg"));
		ImagePanel panel = new ImagePanel((fondo_tablero).getImage());
		frmMojotoe.getContentPane().setForeground(Color.BLACK);
		frmMojotoe.getContentPane().setBackground(new Color(0, 0, 0));
		frmMojotoe.setTitle("MO-JO-TOE");
		frmMojotoe.getContentPane().add(panel);
		frmMojotoe.setBounds(100, 100, 440,435);
		frmMojotoe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMojotoe.getContentPane().setLayout(null);
			
		frmMojotoe.setLocationRelativeTo(null);
		
		listaBotones = new LinkedList<JButton>();
		JButton[] btPosicion_ = new JButton[9];
		for( int i = 0; i <= 8 ;i ++)
		{
			btPosicion_[i] = new JButton("");
			btPosicion_[i].setAlignmentY(Component.BOTTOM_ALIGNMENT);
			btPosicion_[i].setAlignmentX(Component.CENTER_ALIGNMENT);
			btPosicion_[i].setIcon(new ImageIcon(this.getClass().getResource("/casillero_vacio.png")));
			frmMojotoe.getContentPane().add(btPosicion_[i]);		
			listaBotones.add(btPosicion_[i]);
			panel.add(btPosicion_[i]);
		}
		btPosicion_[0].setBounds(43, 29, 106, 77);
		btPosicion_[1].setBounds(159, 29, 106, 77);
		btPosicion_[2].setBounds(275, 29, 106, 77);
		btPosicion_[3].setBounds(43, 117, 106, 77);
		btPosicion_[4].setBounds(159, 117, 106, 77);
		btPosicion_[5].setBounds(275, 117, 106, 77);
		btPosicion_[6].setBounds(43, 205, 106, 77);
		btPosicion_[7].setBounds(159, 205, 106, 77);
		btPosicion_[8].setBounds(275, 205, 106, 77);
		
		for(JButton boton : listaBotones)
		{
			 boton.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent arg0) 
					{
						try
						{
							int i = listaBotones.indexOf(boton);
							accionXBoton( boton, i);	
						}
						catch (Exception e)
						{	
						}
					}
					private void accionXBoton(JButton btPosicion_ , int i) 
					{
						if(presenter.agregarJugada(i)) 
						{
							btPosicion_.setBorder(new MatteBorder(10, 20, 10, 20, (Color) colorActual()));			
							 btPosicion_.setIcon(imagenesBt.imagenjugadorActual());
							juegoFinalizado(presenter);
						}
					}
				});
		}
			
		final JLabel LEC = new JLabel("LEC @borde_luis");
		LEC.setBounds(311, 363, 126, 16);
		LEC.setForeground(Color.RED);
		frmMojotoe.getContentPane().add(LEC);
	
		Button button = new Button("REINICIO");
		button.setBounds(299, 293, 82, 22);
		panel.add(button);
		button.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				frmMojotoe.dispose();
				initialize();
				frmMojotoe.setVisible(true);;
			}
		});	
		JLabel lblNewLabel = new JLabel("LEC @borde_luis");
		lblNewLabel.setForeground(Color.RED);
		lblNewLabel.setBounds(299, 378, 108, 14);
		panel.add(lblNewLabel);
	}
	
	private Color colorActual()
	{
		
		return imagenesBt.getColorActual();
	}
	private void juegoFinalizado(Presenter presenter)
	{	
		try 
		{ if(presenter.getGanador()!=null)
			frmMojotoe.setAlwaysOnTop(false);
			JOptionPane.showMessageDialog(null, presenter.getGanador(), "Hay Ganador!!", JOptionPane.INFORMATION_MESSAGE, imagenesBt.ganadorImagen());
			frmMojotoe.setAlwaysOnTop(true);
		}
		catch (Exception e)
		{	
		}
	}
}


